class Student:
    def __init__(self):
        self.name="Ishu"
        self._rno= 100
        self.__acc="102AB835" 
        print("Parent ")

class Base(Student):
    def __init__(self):
        Student.__init__(self)
        print("Child")
        print(self.name)
        print(self._rno)
        # print(self.__acc)

obj=Base()
print(obj.__acc)