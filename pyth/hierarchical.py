class Person:
    clz="XYZ College"
    def intro(self,name,age,gender):
        print("{} {} years old ({})".format(name,age,gender))

class Student(Person):
    def register(self,course):
        print(course)

class Teacher(Person):
    def details(self,qual,salary):
        print("{} Salary:Rs.{}/-".format(qual,salary))

st1 = Student()
t1=Teacher()

print(st1.clz)
st1.intro("Peter",22,"M")
st1.register("Python")

t1.intro("James",30,"M")
print(t1.clz)
t1.details("PHD",30000)