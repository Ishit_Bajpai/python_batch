class Base:
    comp = "SachTech Solution Pvt. ltd."
    def __init__(self):
        print("Constructor in Base class")
    def info(self,name):
        print(name)

class Child(Base):
    def __init__(self):
        Base.__init__(self)
        print("Constructor in Child class",super().comp)
    def info(self,name):
        print("Name: ",name)
        print("Company: ",self.comp)

obj = Child()
obj.info("Peter")