#Parent class
class Account:
    clz = "Abcd College"
    branch="CSE"
    def fee_detail(self,total,pending):
        print(self.clz)
        print("Total:Rs.{}/-".format(total))
        print("Pending:Rs.{}/-".format(pending))
#Child class
class Student(Account):
    tech="PYTHON"
    def register(self,name,email):
        print("Name:",name)
        print("Email:",email)

ob = Student()
ob.fee_detail(1000,20)
ob.register("aman","aman@gmail.com")
#Update attributes using object
ob.branch="ECE"
print(ob.branch)
ob.tech="AI/ML"
print(ob.tech)