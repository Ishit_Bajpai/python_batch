class Circle:
    pi=3.14
    def area(self,r):
        self.radius = r
        print("radius: ",r)
        return self.pi*r**2

    def param(self):
        print("Radius: ",self.radius)
        return 2*self.pi*self.radius

c1 = Circle()
c2 = Circle()
print(c1.area(10))
print(c2.area(2))
print(c2.param())

    