class Account:
    clz = "Abcd College"
    def fee_details(self,total,pending):
        print("Total:Rs.{}/-".format(total))
        print("Pending:Rs.{}/-".format(pending))

class Library(Account):
    def book_detail(self,bn,isd,rtd):
        print("Book Name: {}\nIssue Date:{}\nReturn Date:{}".
        format(bn,isd,rtd))

class Student(Library):
    tech="PYTHON"
    def register(self,name,email):
        print("Name:",name)
        print("Email:",email)

st1 = Student()
st1.register("Amandeep Kaur","ak@gmail.com")
print(st1.clz)
st1.fee_details(200,0)
st1.book_detail("Python Programming","01-March-2020","15-March-2020")
print(st1.tech)

# st1 = Library()
# print(st1.clz)
# st1.fee_details(200,0)
# st1.book_detail("Python Programming","01-March-2020","15-March-2020")
# st1.register() #parent object can't access prop. of child class