class Student:
    clz_id="AB101" #public
    _clz_name="ABCD college" #protected you just have to add _ at the start of the variable
    # __clz_name="ABCD college" #private you just have to add 2 _ "__" at the start of the variable

    def intro(self,name):
        print(self.__clz_name)
        print("Name:{}".format(name))

obj=Student()
obj.clz_id="CD1111"
print(obj.clz_id)
obj.intro("Ishu")