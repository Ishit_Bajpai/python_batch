str = "Hel2lo 5this12 is8 pyth12on 678676 code!"
def check(x):
    return not x.isdigit()

# ls = list(filter(check,str))
ls = list(filter(lambda a:not a.isdigit(),str))
print("list=",ls)

def abcd(v):
    if v.isdigit():
        return "digit"
    else:
        return v

mp = list(map(abcd,str))
print(mp)
mp1 = list(map(lambda x:"digit" if x.isdigit() else x,str))
print(mp1)