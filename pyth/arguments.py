# """ positional argument
#     keyword arguments
#     default arguments
#     arbitrary arguments
# """

# def student(name,rno,email):
#     print("Name:{}\nRoll no:{}\nEmail:{}".format(name,rno,email))

# student("Peter",1001,"peter@gmail.com")


# student(rno=1001,email="peter@gmail.com",name="Peter")


# def add(*args):
#     print(args)

# add()
# add(10)
# add(1,2,3,4,5)

def mean(*ar):
    b=len(ar)
    ar=list(ar)
    mean=0
    for i in ar:
        mean=mean+i
    mean=mean/b
    return mean

print(mean(10,20,30,40))

def reg(**arg):
    # print(arg)
    for i in arg:
        print("%-10s:%s"%(i,arg[i]))
    print("_____________________")

reg(namr="ishu")
reg(name="ishu",last_name="Baaj")


ab= lambda a:a
print(ab("Hello There"))

ad= lambda a,b:a+b
print(ad(20,30))

check=lambda st: st.isnumeric()
print(check("hello12345"))
print(check("12345"))

greater=lambda a,b: a if a>b else b
print(greater(99,100))
print(greater(9,1))

