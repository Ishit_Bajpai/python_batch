class Base1:
    a = "a in base1"
    def info1(self):
        print("Member function in base 1")

class Base2:
    b = "b in base2"
    def info2(self):
        print("Member function in base 2")

class Child(Base1,Base2):
    c = "Hello There"
    def info3(self):
        print("It is main class")

obj = Child()
print(obj.a,obj.b,obj.c)
obj.info3()
obj.info1()
obj.info2()
print(issubclass(Base1,Child))
# ob = Base2()
# # print(ob.a)
# print(ob.b)
# ob.info2()