# a=10
# print(a)
# print(type(a))
# class abc:
#     def __init__(self,me,ne):
#         self.me=me
#         self.ne=ne
# class bcs(abc):
#     def __init__(self,me,ne):super().__init__(self,me,ne)
#     def get(self,m,n):
#         self.me=m 
#         self.ne=n 
#         print(self.me,self.ne)   

# if __name__ == "__main__":
#     c=bcs
#     c.get(abc,100,20)

ab = "This is py"
print(ab)

#Indexing
# print(ab[1])
# print(ab[-1])
# print(ab[0])
# print(len(ab))

#Slicing
# print(ab[2:])
print(ab[-2:])
# print(ab[:2])
print(ab[:-2])

#Step Size
# print(ab[::1])
# print(ab[::2])
# a = "0123456789"
# print(a[::2])
# print(a[::3])
# print(a[::-1])
# print(ab[::-1])

# print(dir(ab))