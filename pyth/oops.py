#creating class
college = "XYZ Engg. college"
class Student:
    college = "Abcd Engg. College"
    branch = "CSE"

    def intro(self,name,rno):
        print("Name:{} Roll No.: {}".format(name,rno))

#creating object
s1 = Student()
print(s1.college)
print(s1.branch)
s1.intro("Peter",1)
